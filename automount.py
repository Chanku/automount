#!/usr/bin/python3
import time
import os
import subprocess
import socket
import select
import sys

POLL_TIME_INCREASE = 0 #Set the amount of time poll will increase between polls of no change, 0 means no increase
POLL_TIME_MIN = 15 #The minimum limit for the time between polls
POLL_LIMIT = 30 #Maximum amount of seconds between each poll
BLACK_LIST = [] #Device/Partition Blacklists
DIR_CHANGE = {}
MOUNTED_DEVICES = [] #list of already mounted devices

def mount_device(mount_path, device):
    mount_string = "mount /dev/{} {}".format(device, mount_path)
    mount_process = subprocess.Popen(mount_string.split(), stdout=subprocess.PIPE)
    output, error = mount_process.communicate()
    output = output.decode('utf-8')
    try:
        error = error.decode('utf-8')
    except AttributeError:
        error = "None"
    if "does not exist" in output.lower() or "does not exist" in error.lower():
        return False
    else:
        return True

def is_partition(dev):
    if dev.startswith('sd'):
        try:
            int(dev[3:])
            return True
        except ValueError:
            return False
    else:
        return False


def create_mount_point(mount_path):
    if os.path.exists(mount_path):
        return False
    else:
        os.mkdir(mount_path)
        return True

def remove_mount_point(mount_path):
    if os.path.exists(mount_path):
        os.rmdir(mount_path)

def unmount_device(mount_path):
    mount_process = subprocess.Popen("umount {}".format(mount_path), stdout=subprocess.PIPE, shell=True)
    output, error = mount_process.communicate()
    output = output.decode('utf-8')
    try:
        error = error.decode('utf-8')
    except AttributeError:
        error = "None"
    if "mountpoint not found" in output.lower() or "mountpoint not found" in error.lower():
        return False
    else:
        return True

def command_run(command, connection):
    if "kill" in command:
        for dev in MOUNTED_DEVICES:
            try:
                unmount_device(dev)
                remove_mount_point(dev)
                MOUNTED_DEVICES.remove(dev)
            except:
                connection.shutdown(socket.SHUT_RDWR)
                connection.close()
                os.remove('/tmp/pymount.uds')
        connection.shutdown(socket.SHUT_RDWR)
        connection.close()
        os.remove('/tmp/pymount.uds')
        if len(MOUNTED_DEVICES) > 0:
            print("SOME DEVICES COULD NOT BE UNMOUNTED")
            print("THE FOLLOWING DEVICES ARE STILL MOUNTED:")
            for dev in MOUNTED_DEVICES:
                print("/dev/{}".format(dev))
            print("THEY MUST BE MANUALLY UNMOUNTED!")
            with open('/root/umount_error') as f:
                f.write("Some devices are still mounted. They are listed below\n")
                for dev in MOUNTED_DEVICES:
                    f.write("/dev/{}\n".format(dev))
                f.write("You will have to manually unmount them yourself")
        sys.exit(0)
    elif "umount" in command or "unmount" in command:
        dev = command.split()[1].strip()
        try:
            if dev in DIR_CHANGE:
                mount_location = DIR_CHANGE[dev]
            else:
                mount_location = "/mnt/{}".format(dev)
            if unmount_device(mount_location):
                remove_mount_point(mount_location)
            MOUNTED_DEVICES.remove(mount_location)
        except:
            pass
    elif "mount" in command:
        dev = command.split()[1]
        try:
            if dev in DIR_CHANGE:
                mount_location = DIR_CHANGE[dev]
            else:
                mount_location = "/mnt/{}".format(dev)
            if create_mount_point(mount_location):
                mount_device(mount_location, dev)
                MOUNTED_DEVICES.append(mount_location)
        except:
            pass
    elif "detach" in command:
        dev = command.split()[1]
        try:
            if dev in DIR_CHANGE:
                dev_location = DIR_CHANGE[dev]
            else:
                dev_location = "/mnt/{}".format(dev)
            MOUNTED_DEVICES.remove(dev_location)
        except:
            pass
    elif "attach" in command:
        dev = command.split()[1]
        try:
            if dev in DIR_CHANGE:
                dev_location = DIR_CHANGE[dev]
            else:
                dev_location = "/mnt/{}".format(dev)
            MOUNTED_DEVICES.append(dev_location)
        except:
            pass
    elif "ex_attach" in command:
        _, dev, mount_point = command.split()
        DIR_CHANGE[dev] = mount_point
        MOUNTED_DEVICES.append(mount_point)
    elif "blacklist_add" in command:
        dev = command.split()[1]
        BLACKLIST.append(dev)
    elif "blacklist_remove" in command:
        dev = command.split()[1]
        BLACKLIST.remove(dev)
    elif "special_add" in command:
        _, dev, mount_point = command.split()
        dev = str(dev)
        DIR_CHANGE[dev] = mount_point
    elif "special_remove" in command:
        dev = command.split()
        dev = str(dev)
        try:
            del DIR_CHANGE[dev]
        except KeyError:
            pass

        

def command_send(command, connection):
    if "kill" in command:
        connection.send("kill".encode('utf-8'))
    elif "umount" in command or "unmount" in command:
        connection.send("umount {}".format(command[2]).encode('utf-8'))
    elif "mount" in command:
        connection.send("mount {}".format(command[2]).encode('utf-8'))
    elif "detach" in command:
        connection.send("detach {}".format(command[2]).encode('utf-8'))
    elif "attach" in command:
        connection.send("attach {}".format(command[2]).encode('utf-8'))
    elif "detach" in command:
        connection.send("detach {}".format(command[2]).encode('utf-8'))
    elif "ex_attach" in command:
        connection.send("ex-attach {} {}".format(command[2], command[3]).encode('utf-8'))
    elif "blacklist_add" in command:
        connection.send("blacklist_add {}".format(command[2]).encode('utf-8'))
    elif "blacklist_remove" in command:
        connection.send("blacklist_remove {}".format(command[2]).encode('utf-8'))
    elif "special_add" in command:
        connection.send("special_add {} {}".format(command[2], command[3]).encode('utf-8'))
    elif "special_remove" in command:
        connection.send("special_remove {}".format(command[2]).encode('utf-8'))
    elif "help" in command:
        print("pymount (automount) help")
        print("------------------------")
        print(("\t Automount is a program designed to automatically manage USB device mounting "
               "and unmounting, along with allowing users to manually mount and manage them."))
        print(("This program must run as root, and also uses /mnt as the mount location, with "
               "the directory most devices are mounted to as being /mnt/dev_name where dev_name "
               "is the name of the device and parition in /dev (ex: sdb1, sdb2)"))
        print(("Commands:"))
        print(("attach - attach a mounted device previously detached from this program\n"
               "       args: dev_name"))
        print(("blacklist_add - add a device to the blacklist which will make the auto-mounter ignore it.\n"
               "       args: dev_name"))
        print(("blacklist_remove - removes a device from the blacklist.\n"
               "       args: dev_name"))
        print(("detach - detach a mounted device from this program and have it be unmanaged by this program.\n"
               "       args: dev_name"))
        print(("ex_attach - have this program manage a mounted device that was not mounted by this program.\n"
               "       args: dev_name mount_point"))
        print(("mount - mount a device and make it managed by this program, this ignores blacklist.\n"
               "       args: dev_name"))
        print(("special_add - have this program know that a device has a different mountpoint than convention\n"
               "       args: dev_name mount_point"))
        print(("special_remove - have the program know that a device no longer has a different mountpoint than convention"
               "       args: dev_name"))
        print(("umount - unmount a device manually.\n"
               "       args: dev"))
        print(("unmount - alias for umount"))
        pass
    else:
        return False
    connection.close()
    return True

def check_running():
    check_proc = subprocess.Popen("pidof -x {}".format(sys.argv[0]).split(), stdout=subprocess.PIPE)
    output, _ = check_proc.communicate()
    s_output = output.decode('utf-8')
    if len(s_output.split(" ")) > 1:
        our_pid = os.getpid()
        s_output = s_output.split(str(our_pid))[0]
        output = int(s_output)
    else:
        output = int(s_output)
    if output != os.getpid():
        return True
    else:
        return False

def setup_connection():
    if os.path.exists("/tmp/pymount.uds"):
        client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        client.connect('/tmp/pymount.uds')
        return client
    else:
        print("Somehow this is running without the server running...bravo....report this please.")
        sys.exit(1)

def setup_socket():
    if os.path.exists('/tmp/pymount.uds'):
        os.remove('/tmp/pymount.uds')
    serv_var = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    check_proc = subprocess.Popen("chmod 662 /tmp/pymount.uds".format(sys.argv[0]), 
                                  stdout=subprocess.PIPE, shell=True)
    serv_var.bind('/tmp/pymount.uds')
    serv_var.listen(1)
    return serv_var

is_running = check_running()
if os.getuid() != 0:
    if is_running:
        connection = setup_connection()
        if command_send(sys.argv, connection):
            connection.close()
            # connection.detach()
            sys.exit(0)
        else:
            print("Not a command")
            connection.close()
            # connection.detach()
            sys.exit(1)
    else:
        print("The server isn't running, you must initially run this as root")
else:
    if is_running:
        connection = setup_connection()
        if command_send(sys.argv, connection):
            connection.detach()
            sys.exit(0)
        else:
            print("Not a command")
            connection.detach()
            sys.exit(1)
    else:
        serv_var = setup_socket()
        dev_dir_list = os.listdir('/dev')
        poll_in_seconds = POLL_TIME_MIN #time between polling /dev for new devices
        starting_time = time.time()
        while True:
            current_time = time.time()
            is_readable, _, _ = select.select([serv_var], [], [], 10)
            if is_readable:
                conn, _ = serv_var.accept()
                data = conn.recv(2048).decode('utf-8')
                command_run(data, serv_var)
            else:                
                if current_time - starting_time >= poll_in_seconds:
                    new_dev_dir_list = os.listdir('/dev')
                    if new_dev_dir_list == dev_dir_list:
                        if poll_in_seconds < POLL_LIMIT:  
                            poll_in_seconds += POLL_TIME_INCREASE
                    else:
                        poll_in_seconds = POLL_TIME_MIN
                        device_change = list(set(new_dev_dir_list) ^ set(dev_dir_list))
                        for device in device_change:
                            if device in new_dev_dir_list and device not in BLACK_LIST and is_partition(device):
                                if device in DIR_CHANGE:
                                    mount_location = DIR_CHANGE[device]
                                    dev = device
                                else:
                                    mount_location = '/mnt/{}'.format(device)
                                    dev = device
                                try:
                                    if create_mount_point(mount_location):
                                        MOUNTED_DEVICES.append(mount_location)
                                        mount_device(mount_location, dev)
                                except:
                                    pass
                            elif device in dev_dir_list and device not in BLACK_LIST and is_partition(device):
                                if device in DIR_CHANGE:
                                    mount_location = DIR_CHANGE[device]
                                else:
                                    mount_location = '/mnt/{}'.format(device)
                                try:
                                    if mount_location in MOUNTED_DEVICES:
                                        if unmount_device(mount_location):
                                            remove_mount_point(mount_location)
                                        MOUNTED_DEVICES.remove(mount_location)
                                except:
                                    pass
                    dev_dir_list = new_dev_dir_list
            stating_time = time.time()
