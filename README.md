automount (pymount)
===================
   This program is designed to automatically mount thumbdrives and the like, and to manage them as well on Linux. 

Features
--------
* Automatically mount and unmount USB devices
* Clean-up of all mounted devices
* Ability to remove and add USB devices to the manager even if they weren't mounted with it
* Ability to define custom paths to mount a device too
* Abiity to mount/umount devices as any user


Requirements
------------
1. Python 3.X (Python 3.4 or higher is recommended)
2. A Linux Distribution and Install

Installation
------------
1. Install Python 3.X (Python 3.4 or higher is suggested)  
2. Do `git clone  https://Chanku@bitbucket.org/Chanku/automount.git`  
3. Do `cd automount/`  
4. Do `chmod +x automount.py` (to make sure it is executable)  
5. Run it with either:  
  `nohup sudo ./automount.py &`  
   or  
  `su -c "nohup ./automount.py &"`   
6. That's it!

Usage
-----
To use it at any time just plug in a USB, it's commands are formatted in the following way:  
`./automount.py [command] (device partition) (mount path)`  
Most commands only specify the partition or the command, but some commands require an additional mount path to function.  
Run `./automount.py help` to get a list of the commands and their descriptions, below are a few examples:  
Kill: `./automount.py kill`  
ex_attach: `./automount.py ex_attach sdc3 /home/test_user`  
special_add: `./automount.py special_add sdc3 /home/test_user`  
blacklist_add: `./automount.py black_list_add sdc2`  
unmount: `./automount.py unmount sdc4`  
detach: `./automount.py detach sdc1`  



License
-------
The license for this product is the MIT (Expat) License, it's text can be found in the LICENSE file



Contributing
------------
If you want to contribute just fork it and do whatever you want. I'll probably accept the pull request, I just would like to keep it in one file as this is primarily a script, not a library (for obvious reasons). Also don't add a Code of Conduct or a different license without having good reason for it.



PFAQ (Potentially Frequently Asked Questions)
---------------------------------------------
### Why Python 3?  
It's really all I know.

### Why write this?  
Because I wanted it and was unable to connect to the internet at the time.

### Why not license it under GPL v2/3?  
Because I don't like them, also this is a shitty little program/script for a specific thing that has been done so many times over and done many times better. If someone can be successful with this program, then they deserve it.

### Should I use this program?
Probably not. While I do use it, there are better programs to do what this does out there.

### How does this work?  
Basically it just reads from /dev and checks to see if a device has been plugged in, if it has it tries to mount it under /mnt as the name of the partition (assuming the sd[az] convention). So partition 1 on device sdb would be mounted as /mnt/sdb1. If you specify a different directory and name, it will follow that instead. 

### Why do you use /mnt/sd\*?  
Because /mnt existed on my system and because it felt like the sanest and simplest thing to do.

### What are some better programs?
For the being able to manually manage thumbdrives, I would suggest using Steve Litt's choosethumb shell script. It can be found on the DNG Mailing List \(Here is the [code](https://lists.dyne.org/lurker/message/20170409.232704.0dc293a9.en.html) and the [documentation](https://lists.dyne.org/lurker/message/20170220.012754.751e93b2.en.html))   

For the automounting thumb drives I would suggest running a script created by Steve Litt as well, however at this time I am unable to find it
